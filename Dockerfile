FROM --platform=linux/amd64 ubuntu:20.04

RUN apt-get update \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y nginx curl zip unzip git software-properties-common supervisor sqlite3 libxrender1 libxext6 mysql-client libssh2-1-dev autoconf libz-dev\
    && add-apt-repository -y ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y php8.1-fpm php8.1-cli php8.1-gd php8.1-mysql php8.1-intl php8.1-pgsql \
       php8.1-imap php-memcached php8.1-mbstring php8.1-xml php8.1-curl \
       php8.1-sqlite3 php8.1-zip php8.1-pdo-dblib php8.1-bcmath php8.1-ssh2 php8.1-dev php8.1-redis php-pear \
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer \
    && mkdir /run/php

RUN apt-get install -y --no-install-recommends --no-install-suggests \
    nginx \
    ca-certificates \
    gettext \
    mc \
    libmcrypt-dev  \
    libicu-dev \
    libcurl4-openssl-dev \
    mysql-client \
    libldap2-dev \
    libfreetype6-dev \
    libfreetype6 \
    libpcre3-dev  \
    curl \
    libpcsclite-dev \
    vim \
    unzip

# extsensions for php
RUN apt-get update && \
    apt-get install -y --no-install-recommends --no-install-suggests \
    php8.1-common \
    php8.1-mongodb \
    php8.1-curl \
    php8.1-intl \
    php8.1-soap \
    php8.1-xml \
    php8.1-bcmath \
    php8.1-mysql \
    php8.1-amqp \
    php8.1-mbstring \
    php8.1-ldap \
    php8.1-zip \
    php8.1-xml \
    php8.1-xmlrpc \
    php8.1-gmp \
    php8.1-ldap \
    php8.1-gd \
    php8.1-dev \
    php8.1-redis \
    php8.1-xmlreader \
    php8.1-dom \
    php8.1-fpm \
    php8.1-imagick \
    php8.1-tokenizer \
    php8.1-posix \
    php8.1-sockets \
    php8.1-iconv \
    php8.1-exif \
    php8.1-ftp \
    php8.1-simplexml \
    php8.1-xmlreader \
    php8.1-xdebug && \
    echo "extension=apcu.so" | tee -a /etc/php/8.1/mods-available/cache.ini
#    php-mcrypt \

# Install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=bin --filename=composer \
    && php -r "unlink('composer-setup.php');"

# Install node.js
RUN apt install -y gpg-agent && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt update && apt install -y nodejs yarn

# set timezone Asia/Almaty
RUN cp /usr/share/zoneinfo/Asia/Almaty /etc/localtime

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log \
	&& ln -sf /dev/stderr /var/log/php8.1-fpm.log

RUN rm -f /etc/nginx/sites-enabled/*
RUN rm -f /etc/nginx/sites-available/*
COPY ./docker_files/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY ./docker_files/nginx/nginx.conf /etc/nginx/nginx.conf

RUN sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/8.1/fpm/php.ini
RUN sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/8.1/cli/php.ini

RUN mkdir -p /etc/nginx/ssl

COPY . /var/www/
RUN mkdir -p /var/run/php && touch /var/run/php/php8.1-fpm.sock && touch /var/run/php/php8.1-fpm.pid

COPY entrypoint.sh /entrypoint.sh

WORKDIR /var/www/
RUN chmod 755 /entrypoint.sh

RUN chown -R www-data:www-data /var/www
RUN chmod 775 /var/www/

EXPOSE 80
CMD ["/entrypoint.sh"]
