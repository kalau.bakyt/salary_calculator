<div class="container-fluid mt-5">
    <div class="row mb-5">
        <div class="col-4">
            <div class="card mt-3">
                <div class="card-header bg-primary text-light">
                        <b>Калькулятор(Второй end point)</b>
                </div>
                <div class="card-body">   
                    <form method="post" id="calcForm" action="http://localhost:9080/api?controller=api&action=calculate">
                        <input type="hidden" name="from_modal" value="0">
                        <div class="form-group">
                            <label class="form-label"><b>Начисленная зарплата</b></label>
                            <input type="text" class="form-control" name="oklad" value="748780" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><b>Календарный год</b></label>
                            <select class="form-control" name="year">
                                <?php 
                                    foreach(range((int)date("Y"), 2021) as $year) {
                                        echo '<option value="'.$year.'">'.$year.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><b>Календарный месяц</b></label>
                            <select class="form-control" name="month">
                                <option value="1">Январь</option>
                                <option value="2">Февраль</option>
                                <option value="3">Март</option>
                                <option value="4">Апрель</option>
                                <option value="5">Май</option>
                                <option value="6">Июнь</option>
                                <option value="7">Июль</option>
                                <option value="8">Август</option>
                                <option value="9">Сентябрь</option>
                                <option value="10">Октябрь</option>
                                <option value="11">Ноябрь</option>
                                <option value="12">Декабрь</option> 

                                <?php 
                                    // for( $i = 1; $i <= 12; $i++ ){
                                    //     $date_str = date('M', strtotime("+ $i+ months"));
                                    //     echo "<option value=$i>".$date_str ."</option>";
                                    // }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><b>Норма дней в месяц</b></label>
                            <input type="number" class="form-control" name="work_days" min="1" max="31" value="22" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><b>Отработанное количество дней</b></label>
                            <input type="number"  class="form-control" name="worked_days" min="1" max="31"  value="22" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label"><b>ИП(с трудовым договором?)</b></label><br>
                            <input type="radio" class="individual_entrepreneur" name="is_individual_entrepreneur" value="1"> Да
                            <input type="radio" class="individual_entrepreneur" name="is_individual_entrepreneur" value="0" checked="checked"> Нет
                        </div>
                        <div id="radio_buttons">
                            <div class="form-group">
                                <label class="form-label"><b>Имеется ли налоговый вычет 1 МЗП ?</b></label><br>
                                <input type="radio" name="is_mzp" value="1"> Да
                                <input type="radio" name="is_mzp" value="0" checked="checked"> Нет
                            </div>
                            <div class="form-group">
                                <label class="form-label"><b>Является ли сотрудник пенсионером?</b></label><br>
                                <input type="radio" name="retireed" value="1"> Да
                                <input type="radio" name="retireed" value="0" checked="checked"> Нет
                            </div>
                            <div class="form-group">
                                <label class="form-label"><b>Является ли сотрудник инвалидом? </b></label>
                                <span class="badge badge-success" style="font-size: 16px;">
                                    <i class="fa fa-wheelchair"></i>
                                </span><br>
                                <input type="radio" class="disabled_person" name="disabled_person" value="1"> Да
                                <input type="radio" class="disabled_person" name="disabled_person" value="0" checked="checked"> Нет
                            </div>
                            <div class="form-group" id="disability_type_select" style="display:none;">
                                <label class="form-label"><b>Укажите группу инвалидности:</b></label><br>
                                <select class="form-control" name="disability_type" >
                                    <option value="1">1 группа</option>
                                    <option value="2">2 группа</option>
                                    <option value="3">3 группа</option>
                                </select>   
                            </div>
                        </div>
                        <div class="form-group" id="multiple_vacation">
                            <label class="form-label"><b>Укажите период отпуска:</b></label><br>
                            <input type="date" name="vacation_begin"> ~ <input type="date" name="vacation_end">
                            <span class="badge badge-success addVacationPeriod" style="font-size: 16px;">
                                    <i class="fa fa-plus"></i>
                            </span><br>
                        </div>
                        <div class="form-group mt-2">
                            <button type="button" id="calcWithSave" class="btn btn-primary">
                                <span class="spinner-border spinner-border-sm" id="calc_btn_spinner" style="display: none"></span>
                                Рассчитать
                            </button>
                            <a href="<?php echo URL; ?>" class="btn btn-danger">Отмена</a>
                        </div>
                    </form> 
                </div>
            </div>
        </div>

        <div class="col-8">
             <div class="alert alert-success mt-3" id="success_alert" role="alert" style="display:none;"></div>
             <div class="alert alert-danger mt-3" id="error_alert" role="alert" style="display:none;"></div>
            <div class="card mt-3">
                <div class="card-header bg-info text-light">
                    <b>Результаты</b>(валюта:KZT)
                </div>
                <div class="card-body">
                    <table class="table table-sm" id="result_table">
                        <thead>
                            <th>#</th>
                            <th>Начисленная зарплата</th>
                            <th>ИПН</th>
                            <th>ОПВ</th>
                            <th>ОСМС</th>
                            <th>ВОСМС</th>
                            <th>СО</th>
                            <th>Зарплата на руки </th>
                        </thead>
                        <tbody>
                            <?php 
                                // echo "<pre>";
                                // print_r($calculated_salaries);
                                // echo "</pre>";
                                foreach($calculated_salaries as $calc){
                                    echo "<tr>
                                            <td>$calc->id</td>
                                            <td>$calc->accrued_salary</td>
                                            <td>$calc->ipn</td>
                                            <td>$calc->opv</td>
                                            <td>$calc->osms</td>
                                            <td>$calc->vosms</td>
                                            <td>$calc->so</td>
                                            <td><b>$calc->salary_on_hand</b></td>
                                        </tr>";  
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="btn-rcalc" data-toggle="tooltip" data-placement="left" title="Зарплатный калькулятор">
    <i class="fa fa-calculator"></i>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rcalcModal" id="rcalcModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary text-light">
        <h4 class="modal-title">Зарплатный калькулятор(Первый end point)</h4>
        <button type="button float-right" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <form method="POST" id="modalCalcForm">
            <input type="hidden" name="from_modal" value="1">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-label"><b>Оклад</b></label>
                        <input type="text" class="form-control" name="oklad" id="oklad" value="748780" required>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><b>Календарный год</b></label>
                        <select class="form-control" name="year">
                            <?php 
                                foreach(range((int)date("Y"), 2021) as $year) {
                                    echo '<option value="'.$year.'">'.$year.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><b>Календарный месяц</b></label>
                        <select class="form-control" name="month">
                            <option value="1">Январь</option>
                            <option value="2">Февраль</option>
                            <option value="3">Март</option>
                            <option value="4">Апрель</option>
                            <option value="5">Май</option>
                            <option value="6">Июнь</option>
                            <option value="7">Июль</option>
                            <option value="8">Август</option>
                            <option value="9">Сентябрь</option>
                            <option value="10">Октябрь</option>
                            <option value="11">Ноябрь</option>
                            <option value="12">Декабрь</option> 
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><b>Норма дней в месяц</b></label>
                        <input type="number" class="form-control" name="work_days" id="work_days" min="1" max="31" value="22" required>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label class="form-label"><b>Отработанное количество дней</b></label>
                        <input type="number"  class="form-control" name="worked_days" id="worked_days" min="1" max="31"  value="22" required>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><b>ИП(с трудовым договором?)</b></label><br>
                        <input type="radio" class="individual_entrepreneur_modal" name="is_individual_entrepreneur" value="1"> Да
                        <input type="radio" class="individual_entrepreneur_modal" name="is_individual_entrepreneur" value="0" checked="checked"> Нет
                    </div>
                    <div id="radio_buttons_modal">
                        <div class="form-group">
                            <label class="form-label"><b>Имеется ли налоговый вычет 1 МЗП ?</b></label><br>
                            <input type="radio" name="is_mzp" value="1"> Да
                            <input type="radio" name="is_mzp" value="0" checked="checked"> Нет
                        </div>
                        <div class="form-group">
                            <label class="form-label"><b>Является ли сотрудник пенсионером?</b></label><br>
                            <input type="radio" name="retireed" value="1"> Да
                            <input type="radio" name="retireed" value="0" checked="checked"> Нет
                        </div>
                        <div class="form-group">
                            <label class="form-label"><b>Является ли сотрудник инвалидом? </b></label>
                            <span class="badge badge-success" style="font-size: 16px;">
                                <i class="fa fa-wheelchair"></i>
                            </span><br>
                            <input type="radio" class="disabled_person_modal" name="disabled_person" value="1"> Да
                            <input type="radio" class="disabled_person_modal" name="disabled_person" value="0" checked="checked"> Нет
                        </div>
                        <div class="form-group" id="disability_type_select_modal" style="display:none;">
                            <label class="form-label"><b>Укажите группу инвалидности:</b></label><br>
                            <select class="form-control" name="disability_type" >
                                <option value="1">1 группа</option>
                                <option value="2">2 группа</option>
                                <option value="3">3 группа</option>
                            </select>   
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <button type="button" id="show_result" class="btn btn-success">
                        <span class="spinner-border spinner-border-sm" id="show_result_spinner" style="display: none"></span>
                        Рассчитать
                    </button>
                </div>
                <div class="col-8">
                <div class="alert alert-success" id="modal_success_alert" role="alert" style="display:none;"></div>
                <div class="alert alert-danger" id="modal_error_alert" role="alert" style="display:none;"></div>
                </div>
            </div>
        </form>
        <div id="modal_result_blok" style="display:none;">
            <hr>
            <div class="card">
                <div class="card-header bg-warning">
                    Результат(Валюта: KZT)
                </div>
                <div class="card-body">
                <table class="table table-sm">
                    <thead>
                        <th>Начисленная зарплата</th>
                        <th>ИПН</th>
                        <th>ОПВ</th>
                        <th>ОСМС</th>
                        <th>ВОСМС</th>
                        <th>СО</th>
                        <th>Зарплата на руки</th>
                    </thead>
                    <tbody></tbody>
                </table>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
