<?php

require APP . 'model/Calculator.php';

class Api extends Controller
{
    public function index(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: access");
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Allow-Credentials: true");
        header("Content-Type: application/json");

        $product_arr = array(
            "id" =>  1,
            "name" => "fdafdaf",
            "description" => "fdafdaf",
            "price" => "fdafdaf",
            "category_id" => "fdafdaf",
            "category_name" => "fdafdaf"
        );


        http_response_code(200);

        echo json_encode($product_arr);
    }

    public function calculate(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: access");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Credentials: true");
        header("Content-Type: application/json");

        $required = ['oklad', 'year', 'month', 'work_days', 'worked_days','is_mzp', 'disabled_person', 'retireed','is_individual_entrepreneur'];
        $messages = array();
        $vacation_time = '';
        $from_modal = 0;

        foreach($required as $req){
            if(!isset($_POST[$req])){
                $messages[] = "$req, это обязательная поля !";
            }else{
                if($_POST[$req] == ''){
                    $messages[] = "Поля $req нельзя быть пустым!"; 
                }
            }
        }

        if(empty($messages)){
            $oklad = round((float) str_replace(',', '.', $_REQUEST['oklad']), 2);      
            $year = $_REQUEST['year'];
            $month = $_REQUEST['month'];
            $work_days = $_REQUEST['work_days'];
            $worked_days = $_REQUEST['worked_days'];
            $is_individual_entrepreneur = $_REQUEST['is_individual_entrepreneur'];
            $is_mzp = $_REQUEST['is_mzp'];
            $retireed = $_REQUEST['retireed'];
            $disabled_person = $_REQUEST['disabled_person'];
            $disability_type = $_REQUEST['disability_type'];

            if(isset($_POST['from_modal'])){
                $from_modal = $_REQUEST['from_modal'];
            }

            if(isset($_POST['vacation_time'])){
                $vacation_time = $_REQUEST['vacation_time'];
            }
            
            if($is_individual_entrepreneur){
                $is_mzp = 0; $retireed = 0; $disabled_person  = 0;
            }

        /****** business logic begin ********/
            $ipn = 0;
            $opv = 0;
            $vosms = 0;
            $osms = 0;
            $so = 0;
            $mrp = 0;
            $mzp = 0;
            $salary_on_hand = 0;
            $accrued_salary = 0;

            $arr_mrp = [
                2022 => 3063, // МРП за 2022
                2021 => 2917  // МРП за 2021
            ];
            define('MRP', $arr_mrp);

            $arr_mzp = [
                2022 => 60000,
                2021 => 42500
            ];
            define('MZP', $arr_mzp);
            
            $mrp = MRP[$year];

            if($is_mzp != false){
                $mzp = MZP[$year];
            }
            
            // ЗП за день
            $zp_per_day = $oklad / $work_days;

            // Начисленная зарплата(ЗП за отработанные дни)
            $accrued_salary = round(($zp_per_day * $worked_days),2);

            // ОПВ
            $opv = round((($accrued_salary * 10) / 100), 2);

            // ОСМС
            $osms = round((($accrued_salary * 2) / 100), 2);

            // ВОСМС
            $vosms = round((($accrued_salary * 2) / 100), 2);

            // СО
            $so = round(((($accrued_salary - $opv) * 3.5) / 100), 2);

            // ИПН(Если заработная плата за месяц меньше 25 МРП)
            if($accrued_salary < (25 * $mrp)){
                $ipn = round(((($accrued_salary - $opv - $vosms - $mzp) * 90) / 100), 2);
                if($ipn < 0){
                    $ipn = 0;
                }
            }else{
                $ipn = round(((($accrued_salary - $opv - $vosms - $mzp) * 10) / 100), 2);
            }

            // Пенсионер облагается лишь ИПН;
            if($retireed){
                $opv = 0; $vosms = 0; $osms = 0; $so = 0;
                // Пенсионер с инвалидностью не облагается налогами;
                if($disabled_person){
                    $ipn = 0;
                }
            }

            if($disabled_person){
                // Инвалид 1 и 2 группы облагается лишь СО;
                if($disability_type == 1 || $disability_type == 2){
                    // Если ЗП у инвалида превысила 882 МРП, он облагается ИПН;
                    if($accrued_salary > (882 * $mrp)){
                        $opv = 0; $vosms = 0; $osms = 0;
                    }else{
                        $opv = 0; $vosms = 0; $osms = 0; $ipn = 0;
                    }
                }

                // Инвалид 3 группы облагается ОПВ и СО;
                if($disability_type == 3){
                    if($accrued_salary > (882 * $mrp)){
                        $vosms = 0; $osms = 0;
                    }else{
                        $ipn = 0; $vosms = 0; $osms = 0;
                    }
                }
            }

            // Всё расчёты производятся для сотрудника ИП с трудовым договором.
            if($is_individual_entrepreneur){
                $opv = 0; $vosms = 0; $osms = 0; $so = 0; $ipn = 0;
            }
            
            // зарплата на руки
            $salary_on_hand = round(($accrued_salary - $ipn - $opv - $osms - $vosms - $so),2);

        /****** business logic end ********/
            
            $request_data = array(
                "oklad" => $oklad,
                "year" => $year,
                "month" => $month,
                "work_days" => $work_days,
                "worked_days" => $worked_days,
                "is_mzp" => $is_mzp,
                "disabled_person" => $disabled_person,
                "disability_type" => $disability_type,
                "retireed" => $retireed,
                "opv" => $opv,
                "osms" => $osms,
                "vosms" => $vosms,
                "so" => $so,
                "ipn" => $ipn,
                "mrp" => $mrp,
                "mzp" => $mzp,
                "salary_on_hand" => $salary_on_hand,
                "accrued_salary" => $accrued_salary,
                "vacation_time" => $vacation_time,
                "is_individual_entrepreneur" => $is_individual_entrepreneur
            );

            if($from_modal != false){
                http_response_code(200);
                echo json_encode($request_data);
            }else{

                $calc = new Calculator($this->db);
                $calculated_salaries = $calc->saveCalculate($oklad,$year,$month,$work_days,
                                                $worked_days,$is_mzp,$disabled_person,
                                                $disability_type,$retireed,$opv,$osms,
                                                $vosms,$so,$ipn,$mrp,$mzp,$salary_on_hand,
                                                $accrued_salary,$vacation_time,$is_individual_entrepreneur);


                http_response_code(200);

                if($calculated_salaries){ 
                    echo json_encode($calculated_salaries);
                }else{
                    echo json_encode($request_data);
                }
            }          
        }else{
            http_response_code(302);
            echo json_encode($messages);
        }
    }
}